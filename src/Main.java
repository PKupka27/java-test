import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Student student1 = new Student("Test","Test", (byte) 16, "test class" );
        Student student2 = new Student("Test","Test", (byte) 19, "test class" );
        Student student3 = new Student("Test","Test", (byte) 16, "test class" );
        Student student4 = new Student("Test","Test", (byte) 18, "test class" );
        Student student5 = new Student("Test","Test", (byte) 15, "test class" );
        Student student6 = new Student("Test","Test", (byte) 20, "test class" );
        Student student7 = new Student("Test","Test", (byte) 16, "test class" );
        Student student8 = new Student("Test","Test", (byte) 19, "test class" );
        Student student9 = new Student("Test","Test", (byte) 16, "test class" );
        Student student10 = new Student("Test","Test", (byte) 17, "test class" );
        List<Student> allStudents = List.of(student1, student2, student3, student4,student5,student6,student7,student8,student9,student10);

        Clazz classA = new Clazz("A");
        Clazz classB = new Clazz("B");

        for(Student s : allStudents) {
            if(s.getAge() >= 18) {
                classA.getStudents().add(s);
            } else {
                classB.getStudents().add(s);
            }
        }

        classA.getStudents().forEach(student -> {
            student.setClassName(classA.getTitle());
        });

        classB.getStudents().forEach(student -> {
            student.setClassName(classB.getTitle());
        });

        allStudents.forEach(student -> System.out.println(student));

    }
}