import java.util.ArrayList;

public class Clazz {
    private String title;

    private ArrayList<Student> students = new ArrayList();

    public Clazz(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }
}
